defmodule Duper.ResultsDocTest do
  use ExUnit.Case, async: false

  setup_all do
    Duper.Results.reset()

    on_exit(fn -> Duper.Results.reset() end)
  end

  doctest Duper.Results
end
