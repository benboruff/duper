defmodule Duper.Results do
  @doc false
  use GenServer

  @me __MODULE__

  @moduledoc """
  Documentation for Duper.Results API.
  """

  #######
  # API #
  #######

  @doc """
    Start the GenServer.
  """
  def start_link(_) do
    GenServer.start_link(@me, :no_args, name: @me)
  end

  @doc """
  `add_hash_for` adds a file path with it's hash
  as a key to the results list for that key.

  ## Examples

      iex> Duper.Results.add_hash_for("path-to-file", 123456789)
      :ok

  """
  def add_hash_for(path, hash) do
    GenServer.cast(@me, {:add, path, hash})
  end

  @doc """
  `find_duplicates` finds the hashes that have more
  that one path in it's list.

  ## Examples

      iex> Duper.Results.add_hash_for("path-to-x-file", 99999999)
      iex> Duper.Results.add_hash_for("path-to-y-file", 99999999)
      iex> Duper.Results.find_duplicates
      [["path-to-y-file", "path-to-x-file"]]

  """
  def find_duplicates do
    GenServer.call(@me, {:find_duplicates})
  end

  @doc false
  def reset do
    GenServer.cast(@me, {:reset})
  end

  ##################
  # Implimentation #
  ##################

  @doc false
  def init(:no_args) do
    {:ok, %{}}
  end

  def handle_cast({:add, path, hash}, results) do
    results =
      Map.update(
        # map to search
        results,
        # key to find
        hash,
        # if key not found, store with this value
        [path],
        # if key is found, add path to it's list of paths
        fn paths ->
          [path | paths]
        end
      )

    {:noreply, results}
  end

  def handle_cast({:reset}, _results) do
    {:noreply, %{}}
  end

  def handle_call({:find_duplicates}, _from, results) do
    {:reply, hashes_with_more_than_one_path(results), results}
  end

  defp hashes_with_more_than_one_path(results) do
    results
    |> Enum.filter(fn {_hash, paths} -> length(paths) > 1 end)
    # elem/1 is a guard in Kernel, not the Tuple module
    |> Enum.map(&elem(&1, 1))
  end
end
