defmodule Duper.WorkerSupervisor do
  @doc false
  use DynamicSupervisor

  @me __MODULE__

  @moduledoc """
  Documentation for Duper.WorkerSupervisor API.
  """

  #######
  # API #
  #######

  @doc """
    Start a dynamic supervisor for worker processes.
  """
  def start_link(_) do
    DynamicSupervisor.start_link(@me, :no_args, name: @me)
  end

  @doc """
    Spawn a new Duper.Worker server (child process) under the dynamic supervisor.
  """
  def add_worker do
    {:ok, _pid} = DynamicSupervisor.start_child(@me, Duper.Worker)
  end

  ##################
  # Implementation #
  ##################

  @doc false
  def init(:no_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
