defmodule Duper.Worker do
  @doc false
  use GenServer, restart: :transient

  @me __MODULE__

  @moduledoc """
  Documentation for Duper.Worker API.

  The Worker module has no external API
  other than it's start function. The
  worker processes simply compute the hash
  for a path and return it, or report
  there is no more work to do.

  They are stateless.
  """

  def start_link(_) do
    GenServer.start_link(@me, :no_args)
  end

  ##################
  # Implementation #
  ##################

  @doc false
  def init(:no_args) do
    Process.send_after(self(), :do_one_file, 0)
    {:ok, nil}
  end

  def handle_info(:do_one_file, _) do
    Duper.PathFinder.next_path()
    |> add_result
  end

  def add_result(nil) do
    Duper.Gatherer.done()
    {:stop, :normal, nil}
  end

  def add_result(path) do
    Duper.Gatherer.result(path, hash_of_file_at(path))
    send(self(), :do_one_file)
    {:noreply, nil}
  end

  defp hash_of_file_at(path) do
    File.stream!(path, [], 1024 * 1024)
    |> Enum.reduce(
      :crypto.hash_init(:md5),
      fn block, hash ->
        :crypto.hash_update(hash, block)
      end
    )
    |> :crypto.hash_final()
  end
end
