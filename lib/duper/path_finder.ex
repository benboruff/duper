defmodule Duper.PathFinder do
  @doc false
  use GenServer

  @me __MODULE__

  @moduledoc """
  Documentation for Duper.PathFinder API.
  """

  #######
  # API #
  #######

  @doc """
    Start the GenServer for PathFinder.
    The `root` is the starting point in
    the file system to begin traversal.
  """
  def start_link(root) do
    GenServer.start_link(@me, root, name: @me)
  end

  @doc """
    Get the next path in dir tree from DirWalker.
    If there are no more "next path", `nil` is
    returned.
  """
  def next_path() do
    GenServer.call(@me, :next_path)
  end

  ##################
  # Implementation #
  ##################

  @doc false
  def init(path) do
    DirWalker.start_link(path)
  end

  def handle_call(:next_path, _from, dir_walker) do
    path =
      case DirWalker.next(dir_walker) do
        [path] -> path
        # if there is no next path, this will be nil
        other -> other
      end

    {:reply, path, dir_walker}
  end
end
